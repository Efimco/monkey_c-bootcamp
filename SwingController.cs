﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwingController : MonoBehaviour
{
    private Rigidbody _ballRb;
    private FixedJoint _ballJoint;
    private bool _flag = true;
    public float swayingPower;

    void Start()
    {
        _ballJoint = gameObject.GetComponent<FixedJoint>();
        _ballRb = gameObject.GetComponent<Rigidbody>();
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        if (_flag)
        {
            _ballRb.AddForce(Input.GetAxis("Horizontal") * Vector3.forward * swayingPower + Vector3.up);
        }


        if (Input.GetKey(KeyCode.Space))
        {
            Destroy(_ballJoint);
            _flag = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponent<Rigidbody>() != null)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (gameObject.GetComponent<FixedJoint>() == null)
                {
                    _ballJoint = gameObject.AddComponent<FixedJoint>();
                    _ballJoint.connectedBody = other.gameObject.GetComponent<Rigidbody>();
                    _ballJoint.breakForce = Mathf.Infinity;
                    _flag = true;
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Rigidbody>() != null)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (gameObject.GetComponent<FixedJoint>() == null)
                {
                    _ballJoint = gameObject.AddComponent<FixedJoint>();
                    _ballJoint.connectedBody = other.gameObject.GetComponent<Rigidbody>();
                    _ballJoint.breakForce = Mathf.Infinity;
                    _flag = true;
                }
            }
        }
    }
}